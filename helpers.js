export const runOnNextTick = (task = () => {}) => Promise.resolve().then(task);
