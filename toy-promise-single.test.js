import test from "tape";
import ToyPromiseSingle from "./toy-promise1-single.js";
import { runOnNextTick } from "./helpers";

test("methods", (t) => {
  const promise = new ToyPromiseSingle();

  t.equal(typeof promise.then, "function");
  t.equal(typeof promise.resolve, "function");
  t.equal(typeof promise.reject, "function");

  t.end();
});

test("resolving async", async (t) => {
  t.plan(2);

  const promise = new ToyPromiseSingle();
  const expectedArg = "foo";
  let actualArg = null;

  const onFulfilled = (arg) => {
    actualArg = arg;
  };

  promise.then(onFulfilled);
  promise.resolve(expectedArg);

  t.equal(actualArg, null);

  await runOnNextTick();

  t.equal(actualArg, expectedArg);
});

test("rejecting async", async (t) => {
  t.plan(2);

  const promise = new ToyPromiseSingle();
  const expectedArg = "foo";
  let actualArg = null;

  const onRejected = (arg) => {
    actualArg = arg;
  };

  promise.then(undefined, onRejected);
  promise.reject(expectedArg);

  t.equal(actualArg, null);

  await runOnNextTick();

  t.equal(actualArg, expectedArg);
});

test("then when settled async", async (t) => {
  t.plan(2);

  const promise = new ToyPromiseSingle();
  const expectedArg = "foo";
  let actualArg = null;
  const onFulfilled = (arg) => (actualArg = arg);

  promise.resolve(expectedArg);

  await runOnNextTick();

  promise.then(onFulfilled);

  t.equal(actualArg, null);

  await runOnNextTick();

  t.equal(actualArg, expectedArg);
});
