const state = {
  id: "promise-simple",
  initial: "pending",
  states: {
    pending: {
      on: {
        RESOLVE: "settled.fulfilled",
        REJECT: "settled.rejected",
      },
    },
    settled: {
      states: {
        fulfilled: { final: true },
        rejected: { final: true },
      },
    },
  },
};
