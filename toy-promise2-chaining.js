// helper
import { runOnNextTick } from "./helpers";

export default class ToyPromise2 {
  // possible states of a promise
  STATES = {
    pending: "pending",
    fulfilled: "fulfilled",
    rejected: "rejected",
  };

  constructor() {
    // initial state is pending
    this._state = this.STATES.pending;
    this._result = undefined;

    // holds callbacks that run if the promise resolves
    this._fulfillmentTasks = [];
    // holds callbacks that run if the promise rejects
    this._rejectionTasks = [];
  }

  then(onFulfilled, onRejected) {
    // enable chaining
    const resultPromise = new ToyPromise2();

    const fulfillmentTask = () => {
      if (typeof onFulfilled === "function") {
        // get return value from passed in function
        const returned = onFulfilled(this._result);
        // resolve new promise with result
        resultPromise.resolve(returned);
      } else {
        // no onFullfilled passed in
        resultPromise.resolve(this._promiseResult);
      }
    };

    const rejectionTask = () => {
      if (typeof onRejected === "function") {
        const returned = onRejected(this._result);
        resultPromise.resolve(returned);
      } else {
        resultPromise.reject(this._promiseResult);
      }
    };

    switch (this._state) {
      case this.STATES.pending:
        this._fulfillmentTasks.push(fulfillmentTask);
        this._rejectionTasks.push(rejectionTask);
        break;

      case this.STATES.fulfilled:
        // always async!
        runOnNextTick(fulfillmentTask);
        break;

      case this.STATES.rejected:
        runOnNextTick(rejectionTask);
        break;
    }
  }

  resolve(value) {
    // can only be called once - any other calls will be ignored
    if (this._state !== this.STATES.pending) {
      // enable chaining
      return this;
    }

    this._state = this.STATES.fulfilled;
    this._result = value;

    this._clearAndEnqueueTasks(this._fulfillmentTasks);

    // enable chaining
    return this;
  }

  reject(error) {
    // also - can only be called once
    if (this._state !== this.STATES.pending) {
      // enable chaining
      return this;
    }

    this._state = this.STATES.rejected;
    this._result = error;

    this._clearAndEnqueueTasks(this._rejectionTasks);

    return this;
  }

  _clearAndEnqueueTasks(tasks) {
    // clear all subscribed tasks
    this._fulfillmentTasks = null;
    this._rejectionTasks = null;

    tasks.forEach(runOnNextTick);
  }
}
